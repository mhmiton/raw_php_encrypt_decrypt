<?php
    $string = json_encode([
        '1' => 'Mehediul Hassan Miton',
        '2' => 'Mehediul Hassan Miton',
        '3' => 'Mehediul Hassan Miton',
        '4' => 'Mehediul Hassan Miton',
        '5' => 'Mehediul Hassan Miton',
        '6' => 'Mehediul Hassan Miton',
        '7' => 'Mehediul Hassan Miton',
        '8' => 'Mehediul Hassan Miton',
        '9' => 'Mehediul Hassan Miton',
        '10' => 'Mehediul Hassan Miton',
        '11' => 'Mehediul Hassan Miton',
        '12' => 'Mehediul Hassan Miton',
        '13' => 'Mehediul Hassan Miton',
        '14' => 'Mehediul Hassan Miton',
        '15' => 'Mehediul Hassan Miton',
        '16' => 'Mehediul Hassan Miton',
        '17' => 'Mehediul Hassan Miton',
        '18' => 'Mehediul Hassan Miton',
        '19' => 'Mehediul Hassan Miton',
        '14' => 'Mehediul Hassan Miton',
        '20' => 'Mehediul Hassan Miton'
    ]);

    function generate_token($len = 32)
    {
        // Array of potential characters, shuffled.
        $chars = array(
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        );
        shuffle($chars);
        $num_chars = count($chars) - 1;
        $token = '';
        // Create random token at the specified length.
        for ($i = 0; $i < $len; $i++)
        {
            $token .= $chars[mt_rand(0, $num_chars)];
        }
        return $token;
    }

    $secret_key         = 'RTlOMytOZStXdjdHbDZtamNDWFpGdz09';
    $secret_iv          = '7b226e616d65223a224d6568656469756c2048617373616e204d69746f6e227d';
    $encrypt_method     = "AES-256-CBC";
    $key                = hash('sha256', $secret_key);
    $iv                 = hash('sha256', $secret_iv);

    function encrypt($string = null) {
        $octet_1 = base64_encode(openssl_encrypt(generate_token(), $GLOBALS['encrypt_method'], $GLOBALS['key'], 0, $GLOBALS['iv']));
        $octet_2 = base64_encode(openssl_encrypt(generate_token(), $GLOBALS['encrypt_method'], $GLOBALS['key'], 0, $GLOBALS['iv']));

        $enc     = base64_encode(openssl_encrypt($string, $GLOBALS['encrypt_method'], $GLOBALS['key'], 0, $GLOBALS['iv']));
        return $octet_1.'-'.$enc.'-'.$octet_2;
    }

    function decrypt($string = null) {
        $main   = explode('-', $string);
        $dec    = openssl_decrypt(base64_decode($main[1]), $GLOBALS['encrypt_method'], $GLOBALS['key'], 0, $GLOBALS['iv']);
        return $dec;
    }
?>

<!DOCTYPE html>
<html>
<head>
    <title>Encryption & Decryption Use PHP.</title>
</head>
<body style="padding: 10px 50px;">
    <h1>Encryption & Decryption Use PHP.</h1>
    <form action="" method="GET">
        <textarea name="desc" rows="10" style="width: 100%;"><?php echo $string; ?></textarea><br>
        <button type="submit">Submit</button>
    </form>

    <div>
        <?php
            if (isset($_GET['desc'])) {
                echo '
                    <script type="text/javascript">
                        location.replace("index.php?data='.encrypt($_GET['desc']).'");
                    </script>
                ';
            }

            if (isset($_GET['data'])) {
                $data = json_decode(decrypt($_GET['data']));
                echo '<pre>';
                print_r($data);
            }
        ?>
    </div>
</body>
</html>